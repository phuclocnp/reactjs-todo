import React from 'react'
import { Router, Route, hashHistory } from 'react-router'
import Login from './components/Login'
import App from './components/App'
import Whoops404 from './components/Whoops404'

const routes = (
    <Router history={hashHistory}>
        <Route path="/" component={Login} />
        <Route path="/:email" component={App} />
        <Route path="*" component={Whoops404} />
    </Router>
)

export default routes