import NoteRow from './NoteRow'
import list from '../data.json'

const NoteList = ({list, currentNote, onSelect}) => {
  return (
    <div className="note-list">
      <p>Notes</p>
      <div className="scroll-vertical">
        {list.sort((a, b) => {return b.lastUpdated-a.lastUpdated}).map((item, i) => 
          <NoteRow
            key={i} {...item} current={(currentNote && item.createdAt === currentNote.createdAt)} onSelect={() => {onSelect(item)}} />
        )}
      </div>
    </div>
  )
}

export default NoteList