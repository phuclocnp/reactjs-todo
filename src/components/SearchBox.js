import SearchIcon from 'react-icons/lib/io/ios-search-strong'

const SearchBox = () => {
  return (
    <div className="search-box">
      <span className="icon"><SearchIcon /></span>
      <input
        type="text"
      />
    </div>
  )
}

export default SearchBox