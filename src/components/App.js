import { Component } from 'react'
import moment from 'moment'
import SearchBox from './SearchBox'
import NoteList from './NoteList'
import AddIcon from 'react-icons/lib/io/android-add'
import TrashIcon from 'react-icons/lib/fa/trash-o'
import ClockIcon from 'react-icons/lib/io/ios-time-outline'
import Editor from './Editor'
import NoNote from './NoNote'

// Note model: {
//   title,
//   content,
//   createdAt,
//   lastUpdated
// }

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      list: this.loadLocalNotes(),
      currentNote: null
    }
    this.onSaveNote = this.onSaveNote.bind(this)
    this.onAddNote = this.onAddNote.bind(this)
    this.onSelect = this.onSelect.bind(this)
    this.onDeleteNote = this.onDeleteNote.bind(this)
  }

  loadLocalNotes() {
    let array = JSON.parse(localStorage.getItem(this.props.params.email))
    if (Array.isArray(array)) {
      return array
    }
    return []
  }

  onSaveNote(note) {
    let index = this.state.list.map(x => x.createdAt).indexOf(note.createdAt)
    if (index != -1) {
      this.state.list[index] = note
    }
    this.setState({currentNote: note})
  }

  onAddNote() {
    let newNote = {
      title: '',
      content: '',
      createdAt: new Date().getTime(),
      lastUpdated: new Date().getTime()
    }
    this.setState({
      list: [...this.state.list, newNote],
      currentNote: newNote
    })
  }

  onSelect(note) {
    note.current = true
    this.setState({currentNote: note})
  }

  onDeleteNote() {
    if (!this.state.currentNote) return
    let index = this.state.list.map(x => x.createdAt).indexOf(this.state.currentNote.createdAt)
    if (index != -1) {
      this.state.list.splice(index, 1)
    }
    localStorage.setItem("notes", JSON.stringify(this.state.list))
    this.setState({currentNote: null})
  }

  render() {
    return (
      <div className="app">
        <div className="left border">
          <div className="toolbar">
            <SearchBox className="search-box" />
            <a className="tool-item" onClick={this.onAddNote}>
              <AddIcon />
            </a>
          </div>
          <NoteList list={this.state.list} currentNote={this.state.currentNote} onSelect={this.onSelect} />
        </div>
        <div className="right border">
          <div className="toolbar">
            <a className="tool-item" title={(this.state.currentNote) 
                  ? moment(this.state.currentNote.lastUpdated).format('Do-MMMM-YYYY hh:mm:ss')
                  : null} >
              <ClockIcon />
            </a>
            <a className="tool-item" onClick={this.onDeleteNote}>
              <TrashIcon />
            </a>
            <a className="right-text">locnp@dgroup.co</a>
          </div>
          <Editor 
            {...this.state.currentNote}
            current={this.state.currentNote}
            onSaveNote={this.onSaveNote}
            email={this.props.params.email} />
        </div>
      </div>
    )
  }
}

export default App
