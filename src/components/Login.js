import '../stylesheets/login.scss'
import { Component } from 'react'

const KEY_CODE_ENTER = 13

class Login extends Component {

  constructor(props) {
    super(props)
    this.state = {
      email: ''
    }
    this.keyPress = this.keyPress.bind(this)
    this.login = this.login.bind(this)
  }

  keyPress(event) {
    if (event.keyCode == KEY_CODE_ENTER) {
      if (!this.refs.input.value) return
      this.props.router.push('/' + this.refs.input.value)
    }
  }

  login() {
    if (!this.refs.input.value) return
    this.props.router.push('/' + this.refs.input.value)
  }

  render() {
    return (
      <div className="login">
        <p className="title">Enter your email</p>
        <div className="form">
          <input autoFocus={true} ref="input" onKeyPress={this.keyPress} />
        </div>
        <button onClick={this.login}>GO TO YOUR NOTE</button>
      </div>
    )
  }
  
}

export default Login