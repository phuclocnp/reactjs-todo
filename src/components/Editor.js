import { Component, PropTypes } from "react"
import NoNote from "./NoNote"

const TIME_AUTO_SAVE = 1000
const KEY_CODE_ESC = 27

class Editor extends Component {

  constructor(props) {
    super(props)
    this.state = {
      isEdited: false,
      timeout: null,
      tempContent: ''
    }
    this.inputChanged = this.inputChanged.bind(this)
    this.inputBlur = this.inputBlur.bind(this)
    this.keyPress = this.keyPress.bind(this)
    this.saveNote = this.saveNote.bind(this)
  }

  componentDidUpdate(prevProps, prevState) {
    if (!this.props.content && !this.props.current) return
    this.refs.input.value = (this.props.title !== "") ? 
      this.props.title + "\n" + this.props.content :
      this.props.content
  }

  inputChanged(event) {
    this.refs.status.textContent = "Saving..."
    this.state.isEdited = true
    this.state.tempContent = event.target.value
    clearTimeout(this.state.timeout)
    this.state.timeout = setTimeout(() => {
      this.saveNote()
    }, TIME_AUTO_SAVE)
  }

  inputBlur(event) {
    clearTimeout(this.state.timeout)
    this.saveNote()
  }

  componentDidMount(){
    document.addEventListener('keydown', this.keyPress, false);
  }
  componentWillUnmount(){
    document.removeEventListener('keydown', this.keyPress, false);
  }

  keyPress(event) {
    if(event.keyCode == KEY_CODE_ESC){
      this.saveNote()
      this.refs.input.blur()
    }
  }

  saveNote() {
    this.refs.status.textContent = "Saved"
    if (!this.state.tempContent || !this.state.isEdited) return

    let note = {
      title: "",
      content: this.state.tempContent,
      createdAt: this.props.createdAt,
      lastUpdated: new Date().getTime()
    }
    if (this.state.tempContent.indexOf("\n")) {
      note.title = this.state.tempContent.substr(
        0,
        this.state.tempContent.indexOf("\n")
      )
      note.content = this.state.tempContent.substr(
        this.state.tempContent.indexOf("\n") + 1
      )
    }
    let notes = JSON.parse(localStorage.getItem(this.props.email))
    if (Array.isArray(notes)) {
      let index = notes.map(x => x.createdAt).indexOf(this.props.createdAt)
      if (index != -1) {
        notes[index] = note
      } else {
        notes.push(note)
      }
    } else {
      notes = [note]
    }
    localStorage.setItem(this.props.email, JSON.stringify(notes))
    this.props.onSaveNote(note)
  }

  render() {
    return (!this.props.content && !this.props.current) ? <NoNote /> : (
      <div className="editor">
        <div className="status-bar">
          <span className="right-text" ref="status">
            Saved
          </span>
        </div>
        <textarea
          onChange={this.inputChanged}
          onBlur={this.inputBlur}
          onKeyPress={this.keyPress}
          ref="input"
          autoFocus={this.props.content || this.props.current}
          defaultValue={(this.props.title !== "") ? 
            this.props.title + "\n" + this.props.content :
            this.props.content
          } />
      </div>
    )
  }
}

Editor.PropTypes = {
  title: PropTypes.string.isRequired,
  content: PropTypes.string.isRequiredm,
  createdAt: PropTypes.number.isRequired,
  onSaveNote: PropTypes.func.isRequired
}
Editor.defaultProps = {
  title: "",
  content: "",
  createdAt: 0
}

export default Editor
