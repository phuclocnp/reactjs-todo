const NoNote = () => (
  <div className="no-note">
    <span>No note chosen yet</span>
  </div>
)

export default NoNote