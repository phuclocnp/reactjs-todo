const NoteRow = ({ title, content, current, onSelect }) => {
  return (
    <div className={(current) ? "note-row active" : "note-row"}>
      <a onClick={onSelect}>
        <p className="title">{title}</p>
        <p className="content">{content}</p>
      </a>
    </div>
  )
}

export default NoteRow
