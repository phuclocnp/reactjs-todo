import React from 'react'
import { render } from 'react-dom'
import routes from './routes'

import './stylesheets/global.scss'
import './stylesheets/style.scss'
import './stylesheets/editor.scss'
import './stylesheets/note-list.scss'
import './stylesheets/toolbar.scss'

window.React = React

render(
  routes,
  document.getElementById("react-container")
)
